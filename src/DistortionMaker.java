/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Scanner;

/**
 *
 * @author Revan
 */
public class DistortionMaker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Would you like:\n1. A reference image\n2. An animated sequence\n3. Swirl Layer");
        int choice = in.nextInt();
        int size = 0;

        switch (choice) {
            case 1:
                System.out.println("Enter Size(64,128,256...2048 etc: ");
                size = in.nextInt();
                BaseLayer image = new BaseLayer();
                image.buildReference(size);
                break;
            case 2:
                AnimatedRipple ripple = new AnimatedRipple();
                System.out.println("Enter Frames: ");
                int frames = in.nextInt();
                System.out.println("Enter Size(64,128,256...2048 etc: ");
                size = in.nextInt();
                ripple.animateRipple(frames, size);
                break;
            case 3:
                System.out.println("Enter Size(64,128,256...2048 etc: ");
                size = in.nextInt();
                System.out.println("\nEnter Angle(0-360 : ");
                float angle = in.nextFloat();
                SwirlLayer imageSwirl = new SwirlLayer();
                imageSwirl.buildReference(size, angle);
                break; 
            default:
                System.out.println("Goodbye..wait what?");

        }

    }

}
