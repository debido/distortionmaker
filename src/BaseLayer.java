

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import static java.lang.Math.*;
import javax.imageio.ImageIO;
import org.lwjgl.util.vector.Vector2f;

public final class BaseLayer {

    int transformToInt(float vectorComponent) {
        return (int) ((vectorComponent + 1f) * 127.5f);
    }

    public void buildReference(Integer size) {
        String filename = "Reference" + size.toString() + ".PNG";
        File file = new File(filename);
        String format = "PNG";
        int width = size;
        int height = size;
        //(float radius, float minRadius, float maxRadius, int canvasX, int canvasY, int magMin, int magMax)
        float maxRadius = (float)width / 2;
        float radius = (float)width/4;
        //ShaderTorus variableTorus = new ShaderTorus(0f,radius,maxRadius,width,height,0,255);

        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Vector2f center = new Vector2f(width / 2 - 1, height / 2 - 1);

        // [0, 0] refers to the top left, I believe
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {

                Vector2f vectorFromCenter = new Vector2f(0, 0);
                if (x == (width/2-1) && y == (height/2-1)) {
                    vectorFromCenter.x = 0;
                    vectorFromCenter.y = 0;
                } else {
                    vectorFromCenter.x = x;
                    vectorFromCenter.y = y;
                }
                Vector2f.sub(vectorFromCenter, center, vectorFromCenter);
                vectorFromCenter.x *= -1f;
                vectorFromCenter.normalise();
                // vectorFromCenter contains the direction vector from, you guessed it, the center of the image

                int red = transformToInt(vectorFromCenter.x);
                int green = transformToInt(vectorFromCenter.y);
                int blue = 0;
                blue = getDefaultBlue(x, y, width, height);
                //blue = variableTorus.getValue(x, y);
                int alpha = 255;
                image.setRGB(x, y, (alpha << 24) | (red << 16) | (green << 8) | blue);
            }
        }

        try {
            ImageIO.write(image, format, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getDefaultBlue(int x, int y, int width, int height) {
        double distanceFromCentre = sqrt((Math.pow((width / 2 - 1) - x, 2)) + Math.pow((height / 2 - 1) - y, 2));
        if (distanceFromCentre > ((width/2) - 1)){
            return 0;
        }
        double magValue = sin(PI*(distanceFromCentre/((width/2)-1))) * 255;
        return (int) Math.round(magValue);
    }
}
