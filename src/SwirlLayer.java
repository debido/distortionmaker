

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import static java.lang.Math.*;
import javax.imageio.ImageIO;
import org.lwjgl.util.vector.Vector2f;

public final class SwirlLayer {

    int transformToInt(float vectorComponent) {
        return (int) ((vectorComponent + 1f) * 127.5f);
    }

    public void buildReference(Integer size, float angle) {
        String filename = "ReferenceSwirl" + size.toString() + ".PNG";
        File file = new File(filename);
        String format = "PNG";
        int width = size;
        int height = size;
        double radians = Math.toRadians(angle);


        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Vector2f center = new Vector2f(width / 2 - 1, height / 2 - 1);

        // [0, 0] refers to the top left, I believe
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {

                Vector2f vectorFromCenter = new Vector2f(0, 0);
                if (x == (width / 2 - 1) && y == (height / 2 - 1)) {
                    vectorFromCenter.x = 0;
                    vectorFromCenter.y = 0;
                } else {
                    vectorFromCenter.x = x;
                    vectorFromCenter.y = y;
                    
                }
                Vector2f.sub(vectorFromCenter, center, vectorFromCenter);
                vectorFromCenter.x *= -1f;
                vectorFromCenter.normalise();
                vectorFromCenter = rotate(vectorFromCenter.x, vectorFromCenter.y, radians);
                // vectorFromCenter contains the direction vector from, you guessed it, the center of the image

                int red = transformToInt(vectorFromCenter.x);
                int green = transformToInt(vectorFromCenter.y);
                int blue = 0;
                blue = getInvertedSineBlue(x, y, width, height);
                //blue = variableTorus.getValue(x, y);
                int alpha = 255;
                image.setRGB(x, y, (alpha << 24) | (red << 16) | (green << 8) | blue);
            }
        }

        try {
            ImageIO.write(image, format, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getInvertedSineBlue(int x, int y, int width, int height) {
        double distanceFromCentre = sqrt((Math.pow((width / 2 - 1) - x, 2)) + Math.pow((height / 2 - 1) - y, 2));
        if (distanceFromCentre > ((width / 2) - 1)) {
            return 0;
        }
        double magValue = (sin(PI * (distanceFromCentre / ((width / 2) - 1))) - 1d) * 255;
        return (int) Math.abs(Math.round(magValue));
    }

    private Vector2f rotate(float x, float y, double angle) {
        double xV = 0.0d;
        double yV = 0.0d;

        xV = x * cos(angle) - y * sin(angle);
        yV = x * sin(angle) + y * cos(angle);

        return new Vector2f((float) xV, (float) yV);
    }
}
