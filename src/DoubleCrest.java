/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static java.lang.Math.*;

public class DoubleCrest {

    private float radius;
    private float minRadius;
    private float maxRadius;
    private int canvasX;
    private int canvasY;
    private int magMin;
    private int magMax;

    public DoubleCrest() {
        this.radius = 128f;
        this.minRadius = 0f;
        this.maxRadius = 255f;
        this.canvasX = 512;
        this.canvasY = 512;
        this.magMin = 0;
        this.magMax = 255;

    }

    public DoubleCrest(float radius, float minRadius, float maxRadius, int canvasX, int canvasY, int magMin, int magMax) {
        this.radius = radius;
        this.minRadius = minRadius;
        this.maxRadius = maxRadius;
        this.canvasX = canvasX;
        this.canvasY = canvasY;
        this.magMin = magMin;
        this.magMax = magMax;

    }

    public int getValue(int x, int y) {
        int magValue = 0;
        double fraction = 0.0d;

        double distanceFromCentre = sqrt((Math.pow((canvasX / 2 - 1) - x, 2)) + Math.pow((canvasY / 2 - 1) - y, 2));
        if (distanceFromCentre > maxRadius || distanceFromCentre < minRadius) {
            return 0;
        }

        if (distanceFromCentre >= minRadius && distanceFromCentre <= radius) {
            fraction = (distanceFromCentre - minRadius) / (radius / minRadius);
        }
        if (distanceFromCentre > radius && distanceFromCentre <= maxRadius) {
            fraction = (distanceFromCentre-radius)/(maxRadius-radius);
        }

        double magDoubleValue = Math.round(sin(0.5 * PI * fraction) * magMax);
        magValue = (int)magDoubleValue;
        return magValue;

    }

    public int getCanvasX() {
        return canvasX;
    }

    public int getCanvasY() {
        return canvasY;
    }

    public int getMagMax() {
        return magMax;
    }

    public int getMagMin() {
        return magMin;
    }

    public float getMaxRadius() {
        return maxRadius;
    }

    public float getMinRadius() {
        return minRadius;
    }

    public float getRadius() {
        return radius;
    }

    public void setCanvasX(int canvasX) {
        this.canvasX = canvasX;
    }

    public void setCanvasY(int canvasY) {
        this.canvasY = canvasY;
    }

    public void setMagMax(int magMax) {
        this.magMax = magMax;
    }

    public void setMagMin(int magMin) {
        this.magMin = magMin;
    }

    public void setMaxRadius(float maxRadius) {
        this.maxRadius = maxRadius;
    }

    public void setMinRadius(float minRadius) {
        this.minRadius = minRadius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

}
